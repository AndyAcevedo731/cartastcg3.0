from django import forms
from .models import Carta

class CartaForm(forms.ModelForm):
    class Meta:
        model = Carta
        fields = ['nombre', 'descripcion', 'precio','ruta_imagen']

        labels = {
            'nombre': 'Nombre',
            'descripcion': 'Descripcion',
            'precio': 'Precio',
            'ruta_imagen': 'Imagen',
        }
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'descripcion': forms.TextInput(attrs={'class': 'form-control'}),
            'precio': forms.TextInput(attrs={'class': 'form-control'}),
            # 'ruta_imagen': forms.ClearableFileInput(attrs={'class': 'form-control'}),
            # 'ruta_imagen': forms.TextInput(attrs={'class': 'form-control'}),
        }
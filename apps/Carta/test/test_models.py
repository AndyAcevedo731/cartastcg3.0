from django.test import TestCase
from django.template.defaultfilters import slugify
from apps.Carta.models  import Carta


class CartaTestCase(TestCase):
    def setUp(self):
        Carta.objects.create(nombre="Bulbasour", descripcion="Pega duro", precio=2000, ruta_imagen="cartas/taco.png")
    
    def test_ingresar_carta(self):
        """La carta se registro correctamente en la BD"""
        carta_1 = Carta.objects.get(nombre="Bulbasour")
        self.assertEqual(carta_1.precio, 2000)



from django.shortcuts import render, redirect, get_object_or_404
from .models import Carta
from .forms import CartaForm
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

#------------- importacines API ---------------------
from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import CartaSerializer
from rest_framework import status

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser


# Create your views here.
class CartaCreate(CreateView):
    model = Carta
    form_class = CartaForm
    template_name = 'Carta/carta_form.html'
    success_url = reverse_lazy("list_cartas")

class CartaList(ListView):
    model = Carta
    template_name = 'Carta/list_cartas.html'
    # paginate_by = 4

class CartaUpdate(UpdateView):
    model = Carta
    form_class = CartaForm
    template_name = 'Carta/carta_form.html'
    success_url = reverse_lazy('list_cartas')

class CartaDelete(DeleteView):
    model = Carta
    template_name = 'Carta/carta_delete.html'
    success_url = reverse_lazy('list_cartas')
    
# --------------------------------------------------------------
    
def catalogo(request):
    lista= Carta.objects.all()
    precio= request.GET.get('precio')
    nombre_carta= request.GET.get('nombre-carta')

    if 'btn-buscarPrecio' in request.GET:
        if precio: 
            lista= Carta.objects.filter(precio=precio)
    elif 'btn-nombre-carta' in request.GET:
        if nombre_carta:
            lista= Carta.objects.filter(nombre__icontains=nombre_carta)
    
    data = {
        'object_list': lista
    }
    return render(request, 'Carta/catalogo.html', data)


def list_cartas(request):
    lista= Carta.objects.all()
    precio= request.GET.get('precio')
    nombre_carta= request.GET.get('nombre-carta')

    if 'btn-buscarPrecio' in request.GET:
        if precio: 
            lista= Carta.objects.filter(precio=precio)
    elif 'btn-nombre-carta' in request.GET:
        if nombre_carta:
            lista= Carta.objects.filter(nombre__icontains=nombre_carta)
    
    data = {
        'object_list': lista
    }
    return render(request, 'Carta/list_cartas.html', data)


#------------------------API-------------------------------
# El decorador @api_view verifica que la solicitud HTTP apropiada 
# se pase a la función de vista. En este momento, solo admitimos solicitudes GET
@api_view(['GET'])
def carta_collection(request):
    if request.method == 'GET':
        cartas = Carta.objects.all()
        serializer = CartaSerializer(cartas, many=True)
        return Response(serializer.data)

@api_view(['GET', 'PUT', 'DELETE'])
def carta_element(request, pk):
    carta = get_object_or_404(Carta, id=pk)

    if request.method == 'GET':
        serializer = CartaSerializer(carta)
        return Response(serializer.data)
    elif request.method == 'DELETE':
        carta.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    
    elif request.method == 'PUT': 
        carta_new = JSONParser().parse(request) 
        serializer = CartaSerializer(carta, data=carta_new) 
        if serializer.is_valid(): 
            serializer.save() 
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    
    
    
@api_view(['GET', 'POST'])
def carta_collection(request):
    if request.method == 'GET':
        cartas = Carta.objects.all()
        serializer = CartaSerializer(cartas, many=True)
        return Response(serializer.data)
    elif request.method == 'POST':
        serializer = CartaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            # Si el proceso de deserialización funciona, devolvemos una respuesta con un código 201 (creado
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        # si falla el proceso de deserialización, devolvemos una respuesta 400
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

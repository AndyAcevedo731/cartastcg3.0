from django.urls import path, include
from . import views
from django.contrib.auth.views import login_required

from rest_framework.urlpatterns import format_suffix_patterns
from apps.Carta import views

urlpatterns = [
    # llamando a la clases 
    
    path('add_carta', views.CartaCreate.as_view(), name="add_carta"),

    path('list_cartas/', views.list_cartas , name='list_cartas'),

    path('edit_carta/<int:pk>', views.CartaUpdate.as_view(), name='edit_carta'),

    path('del_carta/<int:pk>', views.CartaDelete.as_view(), name='del_carta'),

    path('catalogo/', views.catalogo , name='catalogo'),
    
    # api
    path('cartas/',  views.carta_collection , name='carta_collection'),
    path('cartas/<int:pk>/', views.carta_element ,name='carta_element')
]

# urlpatterns = format_suffix_patterns(urlpatterns)
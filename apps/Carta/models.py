from django.db import models

# Create your models here.
class Carta(models.Model):
    nombre = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=100)
    precio = models.IntegerField()
    ruta_imagen = models.ImageField(upload_to='cartas')

    def __str__(self):
        return self.nombre
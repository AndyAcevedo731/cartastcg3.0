from rest_framework import serializers
from .models import Carta

class CartaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Carta
        fields = ('id', 'nombre', 'descripcion', 'precio', 'ruta_imagen')